// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab7PlayerController.h"
#include "Lab7Character.h"


ALab7PlayerController::ALab7PlayerController() {

}

void ALab7PlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();

	InputComponent->BindAxis("Move", this, &ALab7PlayerController::Move);
	InputComponent->BindAxis("Strafe", this, &ALab7PlayerController::Strafe);
}

void ALab7PlayerController::Move(float value)
{
	ALab7Character* character = Cast<ALab7Character>(this->GetCharacter());
	if (character) {
		character->Move(value);
	}
}

void ALab7PlayerController::Strafe(float value)
{
	ALab7Character* character = Cast<ALab7Character>(this->GetCharacter());
	if (character) {
			character->Strafe(value);
	}
}
