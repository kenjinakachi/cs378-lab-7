// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Lab7PlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LAB_7_API ALab7PlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ALab7PlayerController();

protected:
	virtual void SetupInputComponent() override;

private:
	UFUNCTION()
		void Move(float value);

	UFUNCTION()
		void Strafe(float value);
};
