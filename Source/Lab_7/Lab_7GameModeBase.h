// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Lab_7GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class LAB_7_API ALab_7GameModeBase : public AGameModeBase
{
	GENERATED_BODY()

	ALab_7GameModeBase();
	
};
