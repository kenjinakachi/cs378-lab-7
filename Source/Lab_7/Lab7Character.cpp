// Fill out your copyright notice in the Description page of Project Settings.


#include "Lab7Character.h"
#include "Net/UnrealNetwork.h"
#include "Components/TextRenderComponent.h"
// Sets default values
ALab7Character::ALab7Character()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CountText = CreateDefaultSubobject<UTextRenderComponent>(FName("Count Text Component"));
	CountText->SetupAttachment(RootComponent);

	Count = 0;
	DisplayCount();

}

// Called when the game starts or when spawned
void ALab7Character::BeginPlay()
{
	Super::BeginPlay();

	float TimerLength = FMath::RandRange(2.0f, 5.f);
	GetWorld()->GetTimerManager().SetTimer(CountTimerHandle, this, &ALab7Character::IncrementScore, TimerLength, false);
	
} 

void ALab7Character::ApplyMove(float scale)
{
	AddMovementInput(GetActorForwardVector(), scale);
}

void ALab7Character::ApplyStrafe(float scale)
{
	AddMovementInput(GetActorRightVector(), scale);
}

void ALab7Character::IncrementScore()
{
	Count++;
	DisplayCount();
	if (Count >= 10) {
		if (HasAuthority()) {
			NetMulticast_DeclareWinner();
		}
		Server_DeclareWinner(Count);
	}
	else{
		float TimerLength = FMath::RandRange(.05f, 1.f);
		GetWorld()->GetTimerManager().SetTimer(CountTimerHandle, this, &ALab7Character::IncrementScore, TimerLength, false);
	}
}

void ALab7Character::DisplayCount()
{
	FString NewCount = FString("Count: ") + FString::FromInt(Count);
	CountText->SetText(FText::FromString(NewCount));
}

void ALab7Character::OnRep_Count()
{
	DisplayCount();
}

void ALab7Character::LeaveGame()
{
	this->Destroy();
}

void ALab7Character::DeclareWinner()
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Blue, TEXT("Winner Declared"));
	LeaveGame();
}

void ALab7Character::Server_DeclareWinner_Implementation(int32 CurrentCount)
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Green, TEXT("Called by Server"));
	LeaveGame();
}

bool ALab7Character::Server_DeclareWinner_Validate(int32 CurrentCount)
{
	if (CurrentCount >= 10) {
		return true;
	}
	return false;
}

void ALab7Character::NetMulticast_DeclareWinner_Implementation()
{
	GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Red, TEXT("Called by Everyone"));
	LeaveGame();
}

// Called every frame
void ALab7Character::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ALab7Character::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ALab7Character::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ALab7Character, Count);
}

