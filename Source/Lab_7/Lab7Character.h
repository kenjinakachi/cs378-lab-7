// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Lab7Character.generated.h"

UCLASS()
class LAB_7_API ALab7Character : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALab7Character();

	UFUNCTION(BlueprintImplementableEvent)
		void Move(float value);
	UFUNCTION(BlueprintImplementableEvent)
		void Strafe(float value);

protected:

	UPROPERTY(ReplicatedUsing = OnRep_Count)
		int32 Count;

	UPROPERTY(EditAnywhere)
		class UTextRenderComponent* CountText;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable)
		void ApplyMove(float scale);
	UFUNCTION(BlueprintCallable)
		void ApplyStrafe(float scale);
	
	UFUNCTION()
		void IncrementScore();
	UFUNCTION()
		void DisplayCount();
	UFUNCTION()
		void OnRep_Count();

	UFUNCTION()
		void LeaveGame();

	UFUNCTION()
		void DeclareWinner();

	UFUNCTION(Server, Reliable, WithValidation)
		void Server_DeclareWinner(int32 CurrentCount);
	void Server_DeclareWinner_Implementation(int32 CurrentCount);
	bool Server_DeclareWinner_Validate(int32 CurrentCount);

	UFUNCTION(NetMulticast, Reliable)
		void NetMulticast_DeclareWinner();
	void NetMulticast_DeclareWinner_Implementation();

private:
	FTimerHandle CountTimerHandle;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty> & OutLifetimeProps) const override;

};
