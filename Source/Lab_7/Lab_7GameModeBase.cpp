// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab_7GameModeBase.h"
#include "Lab7Character.h"
#include "Lab7PlayerController.h"

ALab_7GameModeBase::ALab_7GameModeBase()
{
	PlayerControllerClass = ALab7PlayerController::StaticClass();

	static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Class'/Game/Blueprints/Lab7CharacterBP.Lab7CharacterBP_C'"));
	if (pawnBPClass.Object) {
		UClass* pawnBP = (UClass*)pawnBPClass.Object;
		DefaultPawnClass = pawnBP;
	}
	else {
		DefaultPawnClass = ALab7Character::StaticClass();
	}
}
